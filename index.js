var start = Date.now();

// task starts
for (var i = 0; i < 100000000; i++);


if (document.getElementById('ventana1') != null) {
    const ventana = document.getElementById('ventana1');

    ventana.addEventListener('click', function handleClick() {
        console.log(`botonPulsado: 1`)
        console.log(`accionesDistintas: botonInicio`);
        console.log(`espaciosUsuario: inicio`);

        var end = Date.now();
        console.log(`Execution time: ${end - start} ms`);
        start = Date.now();
    });
}

if (document.getElementById('ventana2') != null) {
    const ventana = document.getElementById('ventana2');

    ventana.addEventListener('click', function handleClick() {
        console.log(`botonPulsado: 1`)
        console.log(`accionesDistintas: botonNosotros`);
        console.log(`espaciosUsuario: nosotros`);

        console.log(`tareaCompletada: 1`);
        var end = Date.now();
        console.log(`tiempo: ${end - start}`);
        start = Date.now();
    });
}

if (document.getElementById('ventana3') != null) {
    const ventana = document.getElementById('ventana3');

    ventana.addEventListener('click', function handleClick() {
        console.log(`botonPulsado: 1`)
        console.log(`accionesDistintas: botonServicios`);
        console.log(`espaciosUsuario: servicios`);


    });
}

if (document.getElementById('ventana4') != null) {
    const ventana = document.getElementById('ventana4');

    ventana.addEventListener('click', function handleClick() {
        console.log(`botonPulsado: 1`)
        console.log(`accionesDistintas: botonExitos`);
        console.log(`espaciosUsuario: éxitos`);


        console.log(`tareaCompletada: 1`);
        var end = Date.now();
        console.log(`tiempo: ${end - start}`);
        start = Date.now();
    });
}

if (document.getElementById('ventana5') != null) {
    const ventana = document.getElementById('ventana5');

    ventana.addEventListener('click', function handleClick() {
        console.log(`botonPulsado: 1`)
        console.log(`accionesDistintas: botonContacto`);
        console.log(`espaciosUsuario: contacto`);
    });
}

if (document.getElementById('ventana6') != null) {
    const ventana = document.getElementById('ventana6');

    ventana.addEventListener('click', function handleClick() {
        console.log(`botonPulsado: 1`)
        console.log(`accionesDistintas: botonEncuesta`);
        console.log(`espaciosUsuario: encuesta`);
    });
}

if (document.getElementById('botonContacto') != null) {
    const ventana = document.getElementById('botonContacto');

    ventana.addEventListener('click', function handleClick() {
        console.log(`botonPulsado: 1`);
        console.log(`artefactos: 4`);
        console.log(`accionesDistintas: contactoUsuario`);
        console.log(`accionesDistintas: contactoEmail`);
        console.log(`accionesDistintas: contactoAsunto`);
        console.log(`accionesDistintas: contactoMensaje`);
        console.log(`accionesDistintas: botonContacto`);
        console.log(`tareasInstrumentales: 1`);
        console.log(`tareaCompletada: 1`);
        var end = Date.now();
        console.log(`tiempo: ${end - start}`);
        start = Date.now();
    });
}

if (document.getElementById('botonEncuesta') != null) {
    const ventana = document.getElementById('botonEncuesta');

    ventana.addEventListener('click', function handleClick() {
        console.log(`botonPulsado: 9`);
        console.log(`accionesDistintas: botonPregunta1`);
        console.log(`accionesDistintas: botonPregunta2`);
        console.log(`accionesDistintas: botonPregunta3`);
        console.log(`accionesDistintas: botonPregunta4`);
        console.log(`accionesDistintas: botonPregunta5`);
        console.log(`accionesDistintas: botonPregunta6`);
        console.log(`accionesDistintas: botonPregunta7`);
        console.log(`accionesDistintas: botonPregunta8`);
        console.log(`accionesDistintas: botonEncuesta`);
        console.log(`tareasInstrumentales: 1`);
        console.log(`tareaCompletada: 1`);
        var end = Date.now();
        console.log(`tiempo: ${end - start}`);
        start = Date.now();
    });
}

if (document.getElementById('boton1') != null) {
    const boton1 = document.getElementById('boton1');
    boton1.addEventListener('click', function handleClick() {
        console.log(`botonPulsado: 1`);
        console.log(`accionesDistintas: botonVerMas1`);

    });
}

if (document.getElementById('boton2') != null) {
    const boton2 = document.getElementById('boton2');
    boton2.addEventListener('click', function handleClick() {
        console.log(`botonPulsado: 1`);
        console.log(`accionesDistintas: botonVerMas2`);
        });
}

if (document.getElementById('boton3') != null) {
    const boton3 = document.getElementById('boton3');
    boton3.addEventListener('click', function handleClick() {
        console.log(`botonPulsado: 1`);
        console.log(`accionesDistintas: botonVerMas3`);
        });
}

if (document.getElementById('boton4') != null) {
    const boton4 = document.getElementById('boton4');
    boton4.addEventListener('click', function handleClick() {
        console.log(`botonPulsado: 1`);
        console.log(`accionesDistintas: botonVerMas4`);
        });
}

if (document.getElementById('enviar') != null) {
    const enviar = document.getElementById('enviar');
    enviar.addEventListener('click', function handleClick() {
        console.log(`tareaCompletada`);
        console.log(`botonPulsado`);
        var end = Date.now();
        console.log(`tiempo: ${end - start}`);
        start = Date.now();
    });
}

if (document.getElementById('enviar2') != null) {
    const enviar2 = document.getElementById('enviar2');
    enviar2.addEventListener('click', function handleClick() {
        console.log(`tareaCompletada`);
        console.log(`botonPulsado`);
        var end = Date.now();
        console.log(`tiempo: ${end - start}`);
        start = Date.now();
    });
}


if (document.getElementById('nombre') != null) {
    const nombre = document.getElementById('nombre');
    nombre.addEventListener('click', function handleClick() {
        console.log(`artefacto`);
    });
}

if (document.getElementById('subject') != null) {
    const subject = document.getElementById('subject');
    subject.addEventListener('click', function handleClick() {
        console.log(`artefacto`);
    });
}

if (document.getElementById('email') != null) {
    const email = document.getElementById('email');
    email.addEventListener('click', function handleClick() {
        console.log(`artefacto`);
    });
}

if (document.getElementById('message') != null) {
    const message = document.getElementById('message');
    message.addEventListener('click', function handleClick() {
        console.log(`artefacto`);
    });
}
if (document.getElementById('ventana') != null) {
    const inlineCheckbox1 = document.getElementById('inlineCheckbox1');
}

if (document.getElementById('inlineCheckbox1') != null) {
    const inlineCheckbox1 = document.getElementById('inlineCheckbox1');
    inlineCheckbox1.addEventListener('click', function handleClick() {
        console.log(`artefacto`);
    });
}

if (document.getElementById('inlineCheckbox2') != null) {
    const inlineCheckbox2 = document.getElementById('inlineCheckbox2');
    inlineCheckbox2.addEventListener('click', function handleClick() {
        console.log(`artefacto`);
    });
}

if (document.getElementById('inlineCheckbox3') != null) {
    const inlineCheckbox3 = document.getElementById('inlineCheckbox3');
    inlineCheckbox3.addEventListener('click', function handleClick() {
        console.log(`artefacto`);
    });
}

if (document.getElementById('inlineCheckbox4') != null) {
    const inlineCheckbox4 = document.getElementById('inlineCheckbox4');
    inlineCheckbox4.addEventListener('click', function handleClick() {
        console.log(`artefacto`);
    });
}

if (document.getElementById('inlineCheckbox5') != null) {
    const inlineCheckbox5 = document.getElementById('inlineCheckbox5');
    inlineCheckbox5.addEventListener('click', function handleClick() {
        console.log(`artefacto`);
    });
}

// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
    'use strict'
  
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation')
  
    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
      .forEach(function (form) {
        form.addEventListener('submit', function (event) {
          if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
          }
  
          form.classList.add('was-validated')
        }, false)
      })
  })()


  
